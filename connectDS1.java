//=====================================================================
//
//  File:    connectDS.java      
//  Summary: This Microsoft JDBC Driver for SQL Server sample application
//	     demonstrates how to connect to a SQL Server database by 
//	     using a data source object. It also demonstrates how to 
//	     retrieve data from a SQL Server database by using a stored 
//	     procedure.
//
//---------------------------------------------------------------------
//
//  This file is part of the Microsoft JDBC Driver for SQL Server Code Samples.
//  Copyright (C) Microsoft Corporation.  All rights reserved.
//
//  This source code is intended only as a supplement to Microsoft
//  Development Tools and/or on-line documentation.  See these other
//  materials for detailed information regarding Microsoft code samples.
//
//  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
//  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
//  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//  PARTICULAR PURPOSE.
//
//===================================================================== 

import java.sql.*;
import com.microsoft.sqlserver.jdbc.*;

public class connectDS {

	public static void main(String[] args) {
		
		// Declare the JDBC objects.
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			// String connectionUrl = "jdbc:sqlserver://gavar7v208.database.windows.net:1433;databaseName=DataDogs;user=<uid>;password=<pwd>;";
			String connectionUrl = "jdbc:sqlserver://MANGO;databaseName=Ruud;integratedSecurity=true;";
			System.out.println("Connection string: "+connectionUrl);
			con = DriverManager.getConnection(connectionUrl);  
			System.out.println("Connected database successfully...");
			
			stmt = con.createStatement();
			String sql = "SELECT color FROM colors";
			rs = stmt.executeQuery(sql);
			int i = 1;
			while(rs.next()){
				String color = rs.getString("color");
				System.out.println("color " + i + ": " + color);
				i++;
			}
			rs.close();
		}
	        
		// Handle any errors that may have occurred.
		catch (Exception e) {
			System.out.println("Exception occurred");
			e.printStackTrace();
		}

		finally {
			if (rs != null) try { rs.close(); } catch(Exception e) {}
			if (stmt != null) try { stmt.close(); } catch(Exception e) {}
			if (con != null) try { con.close(); } catch(Exception e) {}
		}
	}
}
